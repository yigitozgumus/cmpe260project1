:-include('plannerData.pl').

%conc(L1,L2,L3)
conc([],L,L).
conc(L,[],L).
conc([X|L1],L2,[X|L3]):- conc(L1,L2,L3).

%rmv(X,L1,L2)
rmv(_,[],[]).
rmv(X,[X|T],T1):-
	 rmv(X,T,T1).
rmv(X,[H|T],[H|T1]):-
	 X \= H,
	 rmv(X,T,T1).

%rmvDup(L1,L2)	
rmvDup([],[]).
rmvDup([H|T],[H|T1]):-
	rmv(H,T,T2),
	rmvDup(T2,T1).

%Predicate for getting whole meal list.
mealList(List):- findall(X,meal(X,_,_,_,_),List).
	
%processMeals(+Allergy,?InitialList,-MealList)
processMeals(_,[],[]):-!.
processMeals(Allergy,[HFoods|TFoods],AllergyList):-
	processMeals(Allergy,TFoods,InnerAllergy),
	foodGroup(Allergy,Foods),
	meal(HFoods,IngList,_,_,_),
	(member(X,Foods),member(X,IngList)->conc([HFoods],InnerAllergy,AllergyList);
		conc([],InnerAllergy,AllergyList)),!.

%findAllergyMeals(+AllergyList, ?InitialList, -MealList)
findAllergyMeals(_,[],[]):-!.
findAllergyMeals([],_,[]):-!.
findAllergyMeals([HAllergy|TAllergy],[HFood|TFood],MealList):-
	findAllergyMeals(TAllergy,[HFood|TFood],Meal),
	processMeals(HAllergy,[HFood|TFood],InnerMail),
	conc(InnerMail,Meal,MealTemp),
	rmvDup(MealTemp,MealList),!.

%processLikes(+Likey, ?InitialList, -MealList)	
processLikes(_,[],[]):-!.
processLikes(Likey,[HFoods|TFoods],LikesList):-
	processLikes(Likey,TFoods,InnerLikes),
	meal(HFoods,IngList,_,_,_),
	(member(Likey,IngList) ->conc([HFoods],InnerLikes,LikesList);
		conc([],InnerLikes,LikesList)),!.

%findLikeMeals(+LikesList, ?InitialList, -MealList)
findLikeMeals(_,[],[]):-!.
findLikeMeals([],_,[]):-!.
findLikeMeals([HLike|TLike],[HFood|TFood],MealList):-
	findLikeMeals(TLike,[HFood|TFood],Likeys),
	processLikes(HLike,[HFood|TFood],InnerLikeys),
	conc(InnerLikeys,Likeys,LikeTemp),
	rmvDup(LikeTemp,MealList),!.

%processCalories(+C,?InitialList, -MealList)
processCalories(_,[],[]):-!.
processCalories(C,[HFood|TFood],Calories):-
	processCalories(C,TFood,Meal),
	meal(HFood,_,Calorie,_,_),
	(Calorie > C -> conc([HFood],Meal,Calories);conc([],Meal,Calories)),!.

%processNotEatings(+EatingType,?InitialList,-MealList)
processNotEatings(_,[],[]):-!.
processNotEatings(EatingType,[HFood|TFood],NotEatingList):-
	cannotEatGroup(EatingType,RestrictionList,Calorie),
	findAllergyMeals(RestrictionList,[HFood|TFood],NotEating),
	(not(Calorie = 0) -> 
		processCalories(Calorie,[HFood|TFood],CalorieList),
		conc(NotEating,CalorieList,CalorieTemp),
		rmvDup(CalorieTemp,NotEatingList),!;
		conc([],NotEating,NotEatingList)),!.

%findNotEatingTypeMeals(+EatingTypeList, ?InitialList, -MealList)
findNotEatingTypeMeals(_,[],[]):-!.
findNotEatingTypeMeals([],_,[]):-!.
findNotEatingTypeMeals([HType|TType],[HFood|TFood],MealList):-
	findNotEatingTypeMeals(TType,[HFood|TFood],MealNot),
	processNotEatings(HType,[HFood|TFood],InnerNot),
	conc(InnerNot,MealNot,NotEatingTemp),
	rmvDup(NotEatingTemp,MealList),!.

%findMealsForTime(+TimeInHand, ?InitialList, -MealList)
findMealsForTime(_,[],[]):-!.
findMealsForTime(T,[HFood|TFood],MealList):-
	findMealsForTime(T,TFood,Meal),
	meal(HFood,_,_,Time,_),
	(T >= Time -> conc([HFood],Meal,MealList);conc([],Meal,MealList)),!.
	
	
%findMealsForMoney(+MoneyInHand, ?InitialList, -MealList)
findMealsForMoney(_,[],[]):-!.
findMealsForMoney(M,[HFood|TFood],MealList):-
	findMealsForMoney(M,TFood,Meal),
	meal(HFood,_,_,_,Money),
	(M >= Money -> conc([HFood],Meal,MealList);conc([],Meal,MealList)),!.

%orderLikedList(+LikeMeals, ?InitialList, -MealList)
orderLikedList([],X,X):-!.
orderLikedList(LikedMeals,Initial,MealList):-
	conc(LikedMeals,Initial,MealTemp),
	rmvDup(MealTemp,MealList),!.


%listPersonalList(+CustomerName, -PersonalList)
listPersonalList(X,PersonalList):-
	customer(X,Allergy,Type,Dislike,Like,TimeInHand,MoneyInHand),
	mealList(FullListT),!,
	findMealsForMoney(MoneyInHand,FullListT,MoneyGos),
	findMealsForTime(TimeInHand,MoneyGos,TimesGo),
	findAllergyMeals(Allergy,TimesGo,Allergens),
	subtract(TimesGo,Allergens,AllergenFree),
	findNotEatingTypeMeals(Type,AllergenFree,IamNotEatingThat),
	subtract(AllergenFree,IamNotEatingThat,LikeThemAll),
	findLikeMeals(Dislike,LikeThemAll,NotSoLike),
	subtract(LikeThemAll,NotSoLike,DislikesGone),
	findLikeMeals(Like,DislikesGone,MuchLikes),
	orderLikedList(MuchLikes,DislikesGone,PersonalList),!.

	
